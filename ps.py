#!/usr/bin/python
import os

all_dirs = os.listdir('/proc/')  # It lists every folder and file in /proc/ folder.

for folder in all_dirs:  # For every folder in all the directories do:
    try:  # folder must be an Integral to continue
        folder = int(folder)
        path = '/proc/%d/stat' % (folder)  # path of the process
        stat = open(path, 'r')
        stat_read = stat.read()
        process_list = stat_read.split()  # split the stat file to list of variables
        pid = process_list[0]  # pid is number 1 in the list (minus 1 because it is counted from 0)
        print 'PID:', pid
        ppid = process_list[3]  # ppid is number 3 in the list
        print 'PPID: ', ppid
        comm = process_list[1]  # comm is number 2 in the list
        print 'COMM: ', comm.strip('()')  # strip the comm from brackets
        utime = process_list[13]  # utime is number 13 in the list
        print 'utime: ', utime
        num_threads = process_list[19]  # num_threads is number 20 in the list
        print 'num_threads: ', num_threads
        processor = process_list[38]  # processor is number 39 in the list
        print 'processor: ', processor
        vsize = process_list[22]  # vsize is number 23 in the list
        print 'vsize: ', vsize, '\n'
        stat.close()

    except ValueError: # if folder is a string (not a number) it is not a process therefore we can skip it.
        pass
exit()