#!/usr/bin/python
# This file is threading management for the functions at lib.py

import lib  # library python file with functions for the executed file (espeak.py)
import threading  # for multitasking strace with tail


# ------ Start of Code ------
# --- Threading management ---

strace = threading.Thread(target=lib.stracefunc)  # Thread of strace and writes out to log
tail = threading.Thread(target=lib.tailfunc)  # Threads that analyzes the log in real time and moves it to espeak

strace.start()  # Starts the strace thread
tail.start()  # Starts the tail thread

# ------ End of Code ------
exit()