#!/usr/bin/python
# /\ Shebang!
# |
import os
# ------ START OF CODE ------
# ------ File Reading ------


class Type(object):

    def __init__(self, protocol):  # when initializing the class it reads the type.
        tcp_file = open('/proc/net/tcp', 'r')  # TCP file
        udp_file = open('/proc/net/udp', 'r')  # UDP file
        if protocol == 'tcp':
            self.read = tcp_file.read()
        if protocol == 'udp':
            self.read = udp_file.read()

        if self.read == open('/proc/net/udp').read():
            self.read = self.read.strip(
                'sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt   uid  timeout inode ref '
                'pointer drops')  # I do not want this line in the variable so i am going to strip it from thr variable
        else:
            self.read = self.read.strip('sl  local_address rem_address   st tx_queue rx_queue tr tm->when retrnsmt'
                                        '   uid  timeout inode   ')
            # I do not want this line in the variable so i am going to strip it from thr variable

        self.read.strip('\n')  # Strips the \n string from the later to be list

    def split(self):
        return self.read.split()

tcp = Type('tcp')
udp = Type('udp')

# ------ Groups that will be in use later in-code ------

tcp_list = tcp.split()  # Splits the tcp file to a list
udp_list = udp.split()  # Splits the udp file to a list
tcp_len = len(tcp_list) / 17  # It has 17 words in a line -> number of lines
udp_len = len(udp_list) / 13  # It has 13 words in a line -> number of lines
try:
    tcp_len = int(tcp_len)
    udp_len = int(udp_len)
except:
    print 'TCP or UDP file is corrupt please try again in a couple of minutes.'
    exit()
tcp_state = {}  # TCP states
tcp_PID = {}  # TCP PIDs
udp_PID = {}  # UDP PIDs
tcp_recvq = {}  # TCP Recv_Qs
tcp_sentq = {}  # TCP Sent_Qs
udp_recvq = {}  # UDP Recv_Qs
udp_sentq = {}  # UDP Sent_Qs

# ------ Address function ------


def address(len, list, list_type, list_num):
    group = {}
    for i in range(0, len):  # makes the hex address to decimal
        x = list[i * list_type + list_num]
        a = str(int(x[0:2], 16))
        b = str(int(x[2:4], 16))
        c = str(int(x[4:6], 16))
        d = str(int(x[6:8], 16))
        port = str(int(x[9:13], 16))
        x = d + '.' + c + '.' + b + '.' + a + ':' + port  # connects every part of the address together
        group[i] = x
    return group

# ------ PID Finder by inode function ------


def pid_finder(inode):  # func_name(inode)

    pid_folders = os.listdir('/proc/')  # Proc Folder

    for pid in pid_folders:  # for every process id running do:
        try:
            pid = int(pid)  # must be a number to continue

            fd_dirs = os.listdir('/proc/%s/fd' % pid)   # all the fd folders

            for fd in fd_dirs:  # for every fd folder do:
                fd = str(fd)  # makes the fd a string (to be later put into readlink)
                os_readlink = os.readlink("/proc/%s/fd/%s" % (pid, fd))  # defines readlink variable (folders in fd)
                try:
                    if inode in os_readlink:  # connects the inode to the readlink variable
                        return pid  # returns the pid connected to the inode
                except:  # if pid is not connected to inode
                    pass
        except:  # if it is not a number
            pass

    return ' -  '  # if an error is happening to the code (mostly os errors of not root permissions)
"""

    Function for finding the pids by inode
    it uses the inode to find the pid connected to the process (uses socket)

"""

# ------ Timer finding function ------


def timer(list, len, list_type):
    group = {}
    for i in range(0, len):
        x = list[i * list_type + 5]
        retransmissions = list[i * list_type + 6]
        timeout = list[i * list_type + 8]
        timer_state = x[0:2]
        if timer_state == '00':
            timer_state = 'off '
        if timer_state == '02':
            timer_state = 'keepalive '
        if timer_state == '03':
            timer_state = 'timewait '
        timer_seconds = str(float(int(x[3:11], 16)) / 100)
        group[i] = timer_state + '(' + timer_seconds + '/' + str(int(retransmissions)) + '/' + timeout + ')'
    return group

# ------ TCP ------
# --- Local Address ---

tcp_local = address(tcp_len, tcp_list, 17, 1)


# --- Foreign Address ---

tcp_rem = address(tcp_len, tcp_list, 17, 2)


# --- State ---
for i in range(0, tcp_len):  # Finds the state of a TCP process
    x = tcp_list[i * 17 + 3]  # the state of a tcp process from 0x 1-12 to Words
    if x == '01':
        x = 'ESTABLISHED'
    elif x == '02':
        x = 'SYN_SENT'
    elif x == '03':
        x = 'SYN_RECV'
    elif x == '04':
        x = 'FIN_WAIT1'
    elif x == '05':
        x = 'FIN_WAIT2'
    elif x == '06':
        x = 'TIME_WAIT'
    elif x == '07':
        x = 'CLOSE'
    elif x == '08':
        x = 'CLOSE_WAIT'
    elif x == '09':
        x = 'LAST_ACK'
    elif x == '0A':
        x = 'LISTEN'
    elif x == '0B':
        x = 'CLOSING'
    else:
        x = 'UNKNOWN'
    tcp_state[i] = x  # puts it into a list


# --- PID ---
for i in range(0, tcp_len):
    inode = tcp_list[i * 17 + 9]
    tcp_PID[i] = str(pid_finder(inode))


# --- Recv-Q and Sent-Q ---
for i in range(0, tcp_len):  # translates from hex to decimal
    x = tcp_list[i * 17 + 4]
    sent_q = x[0:8]
    sent_q = str(int(sent_q, 16))
    recv_q = x[9:17]
    recv_q = str(int(recv_q, 16))
    tcp_recvq[i] = recv_q
    tcp_sentq[i] = sent_q


# --- Timer ---

tcp_timer = timer(tcp_list, tcp_len, 17)

# ------ UDP ------
# --- Local Address ---

udp_local = address(udp_len, udp_list, 13, 1)

# --- Foreign Address ---

udp_rem = address(udp_len, udp_list, 13, 2)

# --- PID ---
for i in range(0, udp_len):
    inode = udp_list[i * 13 + 9]
    udp_PID[i] = str(pid_finder(inode))

# --- Recv-Q and Sent-Q ---
for i in range(0, udp_len):  # translates from hex to decimal
    x = udp_list[i * 13 + 4]
    sent_q = x[0:8]
    sent_q = str(int(sent_q, 16))
    recv_q = x[9:17]
    recv_q = str(int(recv_q, 16))
    udp_recvq[i] = recv_q
    udp_sentq[i] = sent_q

# --- Timer ---

udp_timer = timer(udp_list, udp_len, 13)

# ------ Spaces management ------
# --- function ---


def spaces(name, spaces, type):
    for i in range(0, type):
        while spaces - len(name[i]) > 0:
            name[i] += ' '
    return name

# --- groups ---

tcp_local = spaces(tcp_local, 20, tcp_len)
tcp_rem = spaces(tcp_rem, 20, tcp_len)
tcp_state = spaces(tcp_state, 13, tcp_len)
tcp_PID = spaces(tcp_PID, 19, tcp_len)
tcp_recvq = spaces(tcp_recvq, 10, tcp_len)
tcp_sentq = spaces(tcp_sentq, 5, tcp_len)
udp_local = spaces(udp_local, 20, udp_len)
udp_rem = spaces(udp_rem, 34, udp_len)
udp_PID = spaces(udp_PID, 19, udp_len)
udp_recvq = spaces(udp_recvq, 10, udp_len)
udp_sentq = spaces(udp_sentq, 5, udp_len)

# ------ Print the results ------

print 'Active Internet Connections (servers and established)'
print 'Proto   Recv-Q    Sent-Q   Local Address        Foreign Address      State         PID             ' \
      '    Timer'  # Prints the header of the table

for i in range(0, tcp_len):
    print 'tcp \t ', tcp_recvq[i], tcp_sentq[i], tcp_local[i], tcp_rem[i], tcp_state[i], tcp_PID[i], tcp_timer[i]
    # the table for every TCP process

for i in range(0, udp_len):
    print 'udp \t ', udp_recvq[i], udp_sentq[i], udp_local[i], udp_rem[i], udp_PID[i], udp_timer[i]
    # the table for every UDP process

# ------ END OF CODE ------
exit()