#!/usr/bin/python
# This is the library python file which runs and is built only for functions


import os  # to do os commands inside python
import time  # to make delays
import sqlite3  # sql package
import re  # regex - helps to find patterns inside a string

sockets = []

# ------ Functions ------

def stracefunc():
    """
    This Function is the thread to do strace save the output and be active
    """
    print "Writing to log..."  # status (debugging)
    os.system("strace -o /tmp/logfile.log python -m SimpleHTTPServer > nul")


def tailfunc():
    """
    This Function is the thread of reading new lines from the log analyze it and send it to analyzefunc
    """
    print "Reading log..."  # status (debugging)
    global select  # defines a global string as a key
    select = '0'
    time.sleep(2)  # sleep for 2 seconds (lets strace time to load)
    file = open('/tmp/logfile.log', 'r')  # opens the log file
    while True:  # reads everyline and moves it to analyzing
        line = file.readline()
        analyzefunc(line)


def analyzefunc(l):
    """
    This Function is for analyzing the lines given from tailfunc
    and sending them to get their description for the database
    """
    if "socket(" in l:
        if 'SOCK_STREAM' in l:
            databasefunc('socket', 'SOCK_STREAM')
        if 'SOCK_DGRAM' in l:
            databasefunc('socket', 'SOCK_DGRAM')
        if 'SOCK_SEQPACKET' in l:
            databasefunc('socket', 'SOCK_SEQPACKET')
        if 'SOCK_RAW' in l:
            databasefunc('socket', 'SOCK_RAW')
        if 'SOCK_RDM' in l:
            databasefunc('socket', 'SOCK_RDM')
        if 'SOCK_PACKET' in l:
            databasefunc('socket', 'SOCK_PACKET')
        if 'SOCK_CLOEXEC' in l:
            databasefunc('socket', 'SOCK_CLOEXEC')
        if 'SOCK_NONBLOCK' in l:
            databasefunc('socket', 'SOCK_NONBLOCK')
        socketnum = l.split()[-1]
        sockets.append(socketnum)
    if "bind(" in l:
        databasefunc('bind', '0.0.0.0 8000')

    if "listen(" in l:
        syscalltype = l[l.find('(')+1:l.find(',')]  # Number of socket it is listening to
        databasefunc('listen', syscalltype)

    if "accept(" in l:
        ip_addresses = re.findall(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", l)  # searches for a pattern of IPs
        ports = re.findall(r"\b\d{4,5}\b", l)  # searches for a pattern of ports
        port = ''  # validation tool
        address = ''  # validation tool
        try:  # if found ip addresses it should return one
            address = ip_addresses[0]
        except:
            pass
        try:  # if found ports it should return one
            port = ports[0]
        except:
            pass
        if not address == '' and not port == '':  # if there is port and address, send it to databasefunc
            syscalltype = address + ' ' + port
            databasefunc('accept', syscalltype)
        else:
            pass

    if "connect(" in l:
        if 'AF_INET' in l:
            ip_addresses = re.findall(r"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b", l)  # searches for a pattern of IPs
            ports = re.findall(r"\b\d{4,5}\b", l)  # searches for a pattern of ports
            port = ''  # validation tool
            address = ''  # validation tool
            try:  # if found ip addresses it should return one
                address = ip_addresses[0]
            except:
                pass
            try:  # if found ports it should return one
                port = ports[0]
            except:
                pass
            socketnum = l.split()[0]
            socketnum = socketnum[l.find('(')+1:l.find(',')]
            if not address == '' and not port == '':  # if there is port and address, send it to databasefunc
                syscalltype = socketnum + address + ':' + port
                databasefunc('accept', syscalltype)
            else:
                pass
        if 'AF_LOCAL' in l:  # a local connection address (127.0.0.1)
            socketnum = l.split()[0]
            socketnum = socketnum[l.find('(')+1:l.find(',')]
            syscalltype = socketnum + ' local-connection'
            databasefunc('connect', syscalltype)

    if "send("in l or "sendto(" in l:
        socketnum = l.split()[0]
        socketnum = socketnum[l.find('(') + 1:l.find(',')]  # finds socket number
        strings = re.search(r'"(.*?)"', l)  # finds message between quotes
        try:  # if message is not NULL
            message = strings.group()  # groups all the strings inside quotes
            syscalltype = socketnum + ' ' + message
            databasefunc('send', syscalltype)
        except:
            pass

    if "recv("in l or "recvfrom(" in l:
        socketnum = l.split()[0]
        socketnum = socketnum[l.find('(') + 1:l.find(',')]  # finds socket number
        strings = re.search(r'"(.*?)"', l)  # finds message between quotes
        try:  # if message is not NULL
            message = strings.group()  # groups all the strings inside quotes
            syscalltype = socketnum + ' ' + message
            databasefunc('recv', syscalltype)
        except:
            pass

    if "close(" in l:
        socketnum = l.strip('close(')[0]  # finds socket number
        if socketnum in sockets:  # if it is a socket:
            databasefunc('close', socketnum)
            sockets.remove(socketnum)  # because the socket has been closed it will be removed from the active
            # socket list
        else:
            pass

    if "select(" in l:
        global select
        if '(Timeout)' in l and select == '0':
            select = '1'
            socketnum = l.split()[0][l.find('(')+1:l.find(',')]
            databasefunc('select', 'WAITING ' + socketnum)

        if '(Timeout)' in l and select != '0':
            pass

        if '= 1' in l:
            socketnum = l.split()[0][l.find('(')+1:l.find(',')]
            databasefunc('select', 'READY ' + socketnum)



    else:  # if it is not one of those system calls, it does not concerns us so we can pass it
        pass


def databasefunc(syscall, syscalltype):
    """
     This function already know what type of line is it,
     so it only reads its description and moves it for espeak to talk
    """
    sql = sqlite3.connect('espeak.db')  # connect with the database
    c = sql.cursor()  # c is the sql functions manager (commands executor)

    if syscall == 'socket':  # if syscall is socket print its description
        c.execute('''SELECT SOCKET_DESC
                     FROM socket
                     WHERE SOCKET_TYPE='{0}'; '''.format(syscalltype))
        x = c.fetchone()[0]

    if syscall == 'bind':  # if syscall is bind print its description and format it with the right address and port
        c.execute('''SELECT BIND_DESC
                     FROM bind; ''')
        x = c.fetchone()[0]
        address = syscalltype.split()[0]
        port = syscalltype.split()[1]
        x = x.format(address, port)  # I added {0} and {1} to format with address and port

    if syscall == 'listen':  # if syscall is listen print its description and format it with the socket number
        c.execute('''SELECT LISTEN_DESC
                     FROM listen; ''')
        x = c.fetchone()[0]
        x = x.format(syscalltype)  # I added {0} inside the description for insertion of the socket number

    if syscall == 'accept':  # if syscall is accept print its description and format it with the address and port
        c.execute('''SELECT ACCEPT_DESC
                     FROM accept; ''')
        x = c.fetchone()[0]
        address = syscalltype.split()[0]
        port = syscalltype.split()[1]
        x = x.format(address, port)  # I added {0} and {1} to format with address and port

    if syscall == 'connect':  # if syscall is accept print its description and format it with the address and port
        c.execute('''SELECT CONNECT_DESC
                     FROM connect; ''')
        x = c.fetchone()[0]
        socketnum = syscalltype.split()[0]
        connection = syscalltype.split()[1]
        try:
            connection = connection.replace('-', ' ')
        except:
            pass
        x = x.format(socketnum, connection)  # I added {0} and {1} to format with address and port

    if syscall == 'send':
        c.execute('''SELECT SEND_DESC
                     FROM send; ''')
        x = c.fetchone()[0]
        socketnum = syscalltype.split()[0]
        message = syscalltype.split()[1:]
        mess = " ".join(message)
        x = x.format(mess, socketnum)

    if syscall == 'recv':
        c.execute('''SELECT RECV_DESC
                     FROM recv; ''')
        x = c.fetchone()[0]
        socketnum = syscalltype.split()[0]
        message = syscalltype.split()[1:]
        mess = " ".join(message)
        x = x.format(mess, socketnum)

    if syscall == 'close':
        c.execute('''SELECT CLOSE_DESC
                     FROM close; ''')
        x = c.fetchone()[0]
        x = x.format(syscalltype)

    if syscall == 'select':
        systype = syscalltype.split()[0]
        socketnum = syscalltype.split()[1]
        c.execute('''SELECT SELECT_DESC
                     FROM select
                     WHERE SELECT_TYPE='{0}'; '''.format(systype))
        x = c.fetchone()[0]
        x = x.format(socketnum)


    espeakfunc(x)  # sends the out put to the espeak function


def espeakfunc(line):
    """
    This function is given with the description
    of the syscall and is using os.system(command)
    to say it out loud
    """
    os.system('espeak "{0}"'.format(line))