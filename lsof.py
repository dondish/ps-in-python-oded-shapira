#!/usr/bin/python2.7

# --- Packages ---
import os
import pwd
import stat

# --- Lists and paths ---
folders = os.listdir('/proc/')
spainodes = []
spausernames = []
spamodes = []
spatypes = []
spasizes = []
spadevices = []
memspatypes = []
memspasizes = []
memspainodes = []
memspausernames = []
memspadevices = []

# ------ Functions ------


def link_reader(pid, folder):
    """
    finds the link target of
    every file in the fd directory
    (the name)

    """
    try:  # if permissions are given
        names = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):  # for every file in the fd directory
            name = os.readlink('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # read the link target
            if "pipe:" in name:  # if pipe is in the string just print pipe (how it was presented)
                name = "pipe"
            names.append(name)
        return names  # returns all the names

    except OSError:  # if permissions are not met
        return []


def inode_finder(pid, folder):
    """
    finds the inode of
    every file in the fd directory

    """
    try:  # if permissions are given
        inodes = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):   # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # the stat result of every file
            inode = stat_result.st_ino  # the inode in the stat result
            inodes.append(str(inode))
        return inodes  # return the inodes

    except OSError:  # if permissions are not met
        return []


def username_finder(pid, folder):
    """
     finds the username
     through uid given
     by the stat file

    """
    try:  # if permissions are given
        usernames = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):  # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # the stat result of every file
            uid = stat_result.st_uid  # get the uid
            pwuid = pwd.getpwuid(uid)  # get pwuid from the uid
            username = str(pwuid).split()[0]  # get the block with username in it
            username = username[27:]  # strip the username form the block
            username = username.strip("',")  # strip the username form the block
            usernames.append(username)
        return usernames  # return all usernames

    except OSError:  # if permissions are not met
        return []


def cwd_reader(pid):
    """
    reads the cwd
    and returns the
    current working
    directory

    """
    try:  # if permissions are given
        cwd = os.readlink('/proc/{0}/cwd'.format(pid))
        cwd_stat = os.stat('/proc/{0}/cwd'.format(pid))
        cwduid = cwd_stat.st_uid
        cwdusername = str(pwd.getpwuid(cwduid)).split()[0]
        cwdusername = cwdusername[27:].strip("',")
        cwdino = cwd_stat.st_ino
        cwdsize = cwd_stat.st_size
        device = cwd_stat.st_dev
        major = str(os.major(device))
        minor = str(os.minor(device))
        cwddevice = major + "," + minor
        return [cwd, cwdusername, cwdino, cwdsize, cwddevice]

    except OSError:
        return []


def root_reader(pid):
    """
    reads the root
    and returns the
    rtd

    """
    try:   # if permissions are given
        root = os.readlink('/proc/{0}/root'.format(pid))
        root_stat = os.stat('/proc/{0}/root'.format(pid))
        rootuid = root_stat.st_uid
        rootusername = str(pwd.getpwuid(rootuid)).split()[0]
        rootusername = rootusername[27:].strip("',")
        rootino = root_stat.st_ino
        rootsize = root_stat.st_size
        device = root_stat.st_dev
        major = str(os.major(device))
        minor = str(os.minor(device))
        rootdevice = major + "," + minor
        return [root, rootusername, rootino, rootsize, rootdevice]

    except OSError:
        return []


def exe_reader(pid):
    """
    reads the exe
    and returns the
    exe stat

    """
    try:   # if permissions are given
        exe = os.readlink('/proc/{0}/exe'.format(pid))
        exe_stat = os.stat('/proc/{0}/exe'.format(pid))
        rootuid = exe_stat.st_uid
        exeusername = str(pwd.getpwuid(rootuid)).split()[0]
        exeusername = exeusername[27:].strip("',")
        exeino = exe_stat.st_ino
        exesize = exe_stat.st_size
        device = exe_stat.st_dev
        major = str(os.major(device))
        minor = str(os.minor(device))
        exedevice = major + "," + minor
        return [exe, exeusername, exeino, exesize, exedevice]

    except OSError:
        return []


def mode_finder(pid):
    """
    finds the mode of
    every file in the fd directory

    """
    try:  # if permissions are given
        fds = []
        for fd in os.listdir('/proc/{0}/fd/'.format(str(pid))):   # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/fd/{1}'.format(str(pid), fd))  # the stat result of every file
            mode = bool(stat_result.st_mode & stat.S_IRGRP)
            if mode is True:
                mode = 'r'
            else:
                mode = 'u'
            fds.append(str(fd)+ mode)
        return fds

    except OSError:
        return []


def type_finder(pid, folder):
    """
    arranges the files
    to types

    """
    try:  # if permissions are given
        types = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):  # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # the stat result of every file
            mode = stat_result.st_mode
            if stat.S_ISCHR(mode):
                type = 'CHR'
            elif stat.S_ISDIR(mode):
                type = 'DIR'
            elif stat.S_ISFIFO(mode):
                type = 'FIFO'
            elif stat.S_ISLNK(mode):
                type = 'LNK'
            elif stat.S_ISREG(mode):
                type = 'REG'
            elif stat.S_ISSOCK(mode):
                type = 'SOCK'
            else:
                type = 'a_inode'
            types.append(type)
        return types

    except OSError:
        return []


def size_finder(pid, folder):
    """
    finds the size
    of the process

    """
    try:  # if permissions are given
        sizes = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):   # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # the stat result of every file
            size = stat_result.st_size  # the inode in the stat result
            if str(size) == '0':
                size = ''
            sizes.append(str(size))
        return sizes  # return the inodes

    except OSError:  # if permissions are not met
        return []


def device_finder(pid, folder):
    """
    finds the device of
    every file in the fd directory
    NOT FULLY MADE YET

    """
    try:  # if permissions are given
        devices = []
        for fd in os.listdir('/proc/{0}/{1}/'.format(str(pid), folder)):   # for every file in the fd directory
            stat_result = os.stat('/proc/{0}/{1}/{2}'.format(str(pid), folder, fd))  # the stat result of every file
            device = stat_result.st_dev  # the inode in the stat result
            major = os.major(device)
            minor = os.minor(device)
            output = str(major) + "," + str(minor)
            devices.append(output)
        return devices  # return the inodes

    except OSError:  # if permissions are not met
        return []


def command_finder(pid):
    """
    finds the command
    name of every pid

    """
    proccomm = open('/proc/{0}/comm'.format(str(pid)), 'r')  # open the comm file
    command = proccomm.read()  # read the comm file
    command = command.rstrip()  # strip the whitespaces and the blank lines
    proccomm.close()  # close the comm file
    return command  # return the command name

# ------ Spaces management function ------


def spaces(string, spaces):
    """
     adds spaces after
     string so it
     will match the
     amount of charcters as
     bigger and smaller
     strings.

    """
    while spaces - len(string) > 0:
        string += ' '

    if spaces - len(string) < 0:
        string = string[0:spaces]
    return string

# ------ Run for every PID ------
print "COMMAND   PID                  USER         FD TYPE     DEVICE SIZE/OFF      NODE   NAME"

for pid in folders:
    try:  # if folder is integer and not text (process directory)

        pid = int(pid)

        cwd = cwd_reader(pid)  # gets the current working directory of the pid

        root = root_reader(pid)  # gets the rtd of the pid

        exe = exe_reader(pid)  # gets the exe of the pid

        strpid = spaces(str(pid), 20)  # calls spaces management function on the pid

        memnames = link_reader(pid, 'map_files')

        memtypes = type_finder(pid, 'map_files')
        if not memtypes == []:  # if types is not empty
            for type in memtypes:
                type = spaces(type, 8)  # spaces management function
                memspatypes.append(type)
        else:
            memspatypes = []

        memsizes = size_finder(pid, 'map_files')
        if not memsizes == []:
            for size in memsizes:
                size = spaces(size, 14)  # spaces management function
                memspasizes.append(size)
        else:
            memspasizes = []

        meminodes = inode_finder(pid, 'map_files')
        if not meminodes == []:
            for inode in meminodes:
                inode = spaces(inode, 8)  # spaces management function
                memspainodes.append(inode)
        else:
            memspainodes = []

        memusernames = username_finder(pid, 'map_files')  # calls usernames function
        if not memusernames == []:  # if premissions were given
            for username in memusernames:
                username = spaces(username, 12)  # spaces management function
                memspausernames.append(username)
        else:
            memspausernames = []

        memdevices = device_finder(pid, 'map_files')
        if not memdevices == []:
            for device in memdevices:
                device = spaces(device, 7)
                memspadevices.append(device)
        else:
            memspadevices = []

        names = link_reader(pid, 'fd')  # calls names function

        types = type_finder(pid, 'fd')
        if not types == []:  # if types is not empty
            for type in types:
                type = spaces(type, 8)  # spaces management function
                spatypes.append(type)
        else:
            spatypes = []

        sizes = size_finder(pid, 'fd')
        if not sizes == []:
            for size in sizes:
                size = spaces(size, 14)
                spasizes.append(size)
        else:
            spasizes = []

        inodes = inode_finder(pid, 'fd')  # calls inode function
        if not inodes == []:
            for inode in inodes:
                inode = spaces(inode, 8)  # calls spaces management function
                spainodes.append(inode)  # creates a new list with the spaces
        else:
            spainodes = []

        usernames = username_finder(pid, 'fd')  # calls username function
        if not usernames == []:
            for username in usernames:
                username = spaces(username, 12)  # calls spaces management function
                spausernames.append(username)  # creates a new list with the spaces
        else:
            spausernames = []

        devices = device_finder(pid, 'fd')
        if not devices == []:
            for device in devices:
                device = spaces(device, 7)
                spadevices.append(device)
        else:
            spadevices = []

        modes = mode_finder(pid)  # calls mode function on fd
        if not modes == []:
            for mode in modes:
                mode = spaces(mode, 4)
                spamodes.append(mode)
        else:
            spamodes = []

        try:
            command = command_finder(pid)  # calls command function
            command = spaces(command, 9)  # calls spaces management function

        except IOError:  # if it is not loading it is probably python.
            command = spaces('python', 9)

# ------ Output ------
# ------ CWD Output ------

        if not cwd == []:  # if permissions were given
            cwd[1] = spaces(str(cwd[1]), 12)  # calls spaces management function
            cwd[2] = spaces(str(cwd[2]), 8)  # calls spaces management function
            cwd[3] = spaces(str(cwd[3]), 14)  # calls spaces management function
            cwd[4] = spaces(cwd[4], 7)  # calls spaces management function
            print command + " " + strpid + " " + cwd[1]+ " cwd DIR     " + cwd[4] +\
                  cwd[3] + cwd[2] +  cwd[0]
        else:  # else it's root without permissions
            print command + " " + strpid + " root         cwd unknown        " \
                                           "                 /proc/{0}/cwd (cannot readlink)".format(pid)

# ------ Root Output ------

        if not root == []:  # if permissions were given
            root[1] = spaces(str(root[1]), 12)  # calls spaces management function
            root[2] = spaces(str(root[2]), 8)  # calls spaces management function
            root[3] = spaces(str(root[3]), 14)  # calls spaces management function
            root[4] = spaces(root[4], 7)
            print command + " " + strpid + " " + root[1] + " rtd DIR     " + root[4] +\
                  root[3] + root[2] + root[0]
        else:  # else it's root without permissions
            print command + " " + strpid + " root         rtd unknown        " \
                                           "                      /proc/{0}/root (cannot readlink)".format(pid)
# ------ Exe Output ------

        if not exe == []:  # if permissions were given
            exe[1] = spaces(str(exe[1]), 12)  # calls spaces management function
            exe[2] = spaces(str(exe[2]), 8)  # calls spaces management function
            exe[3] = spaces(str(exe[3]), 14)  # calls spaces management function
            exe[4] = spaces(exe[4], 7)  # calls spaces management function
            print command + " " + strpid + " " + exe[1] + " txt REG     " + exe[4] + \
                  exe[3] + exe[2] + exe[0]
        else:  # else it's root without permissions
            print command + " " + strpid + " root         txt unknown        " \
                                           "                      /proc/{0}/root (cannot readlink)".format(pid)
# ------ Map Files Output ------

        if len(memnames) == 0:
            pass
        else:
            for i in range(0, len(memnames)):  # The output:
                if memspasizes[i] == '              ':
                    memspatypes[i] = 'REG     '
                    print command + " " + strpid + " " + memspausernames[i] + ' DEL ' + memspatypes[i] + \
                          memspadevices[i] + memspasizes[i] + memspainodes[i] + memnames[i]
                else:
                    print command + " " + strpid + " " + memspausernames[i] + ' mem ' + memspatypes[i] + \
                          memspadevices[i] + memspasizes[i] + memspainodes[i] + memnames[i]

# ------ FD Output ------

        if len(names) == 0:
            print command + " " + strpid + " root        NOFD unknown        " \
                                           "                       /proc/{0}/fd (cannot opendir)".format(pid)
        else:
            for i in range(0, len(names)):  # The output:
                if spatypes[i] == 'CHR     ' or spatypes[i] == 'FIFO    ' or spatypes[i] == 'SOCK    ':
                    spasizes[i] = '0t0           '
                print command + " " + strpid + " " + spausernames[i] + " " + spamodes[i] + spatypes[i] + \
                       spadevices[i] + spasizes[i] + spainodes[i] + names[i]

    except ValueError:  # else
        pass


# --- after finishing all the PIDs close ---

exit()